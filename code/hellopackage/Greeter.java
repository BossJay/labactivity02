package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;


public class Greeter {

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        Utilities utilities = new Utilities();
        System.out.println("Enter a number");
        int number = scan.nextInt();

        int answer = utilities.doubleMe(number);
        System.out.println(answer);
    }
}